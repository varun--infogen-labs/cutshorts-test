import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');

import SlidingPanel from 'react-native-sliding-up-down-panels';


export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>                      
        
        <View style={{flexDirection: 'row'}}>
          
        </View>
        <View style={styles.bodyViewStyle}>
          <Text>Hello My World</Text>
        </View>
       
        <SlidingPanel
            headerLayoutHeight = {100}
            headerLayout = { () =>
                <View style={styles.headerLayoutStyle}>
                  <Text style={styles.commonTextStyle}>My Awesome sliding panel</Text>
                </View>
            }
            slidingPanelLayout = { () =>
                <View style={styles.slidingPanelLayoutStyle}>
                  <Text style={styles.commonTextStyle}>The best thing about me is you</Text>
                </View>
            }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F1F5F6'
  },
  bodyViewStyle: {
    flex: 1,
    marginTop: '40%',
    justifyContent: 'center', 
    alignItems: 'center',
    borderTopRightRadius: 40,
    backgroundColor: 'white'
  },
  headerLayoutStyle: {
    width, 
    height: 100, 
    backgroundColor: '#1C9AFF', 
    justifyContent: 'center', 
    alignItems: 'center',
    borderTopRightRadius: 40
  },
  slidingPanelLayoutStyle: {
    width, 
    height, 
    backgroundColor: '#1C9AFF', 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  commonTextStyle: {
    color: 'white', 
    fontSize: 18,
  },
});
