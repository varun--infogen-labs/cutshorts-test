import React, { Component } from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Dialog from "react-native-dialog";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import MCIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { connect } from 'react-redux';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { addTodo } from '../actions/index';
import TodoItemWithDescription from '../components/TodoItemWithDescription';
const { width, height } = Dimensions.get('window');


class DetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: '#F1F5F6',
      shadowColor: 'transparent',
      shadowRadius: 0,
      shadowOffset: {
        height: 0,
      },
      elevation: 0,
      borderBottomWidth: 0,
    },
    headerLeft: <MCIcons name='chevron-left' style={{ color: '#46aafd', fontSize: hp(5), paddingLeft: 20 }} onPress={() => navigation.pop()} />,
    headerRight: <View style={{ paddingRight: 20 }}>
      <TouchableOpacity style={{ height: 30, width: 30, padding: 3, borderRadius: 15, alignContent: 'center' }} >
        <Image source={require('../../assets/user.png')} style={{ width: 24, height: 24, alignSelf: 'center' }} />
      </TouchableOpacity>
    </View>
  });

  constructor(props) {
    super(props)

    this.state = {
      listId: '',
      todoTitle: '',
      listTitle: '',
      todoDescription: '',
      todoDialogVisible: false,
      activeList: this.props.navigation.state.params.currentList,
      nextList: this.props.lists.length > this.props.navigation.state.params.currentList.id ? this.props.lists[this.props.navigation.state.params.currentList.id] : this.props.lists[this.props.navigation.state.params.currentList.id - 2],
      currentListColor: this.props.navigation.state.params.currentList.backgroundColor,
      nextListColor: this.props.lists.length > this.props.navigation.state.params.currentList.id ? this.props.lists[this.props.navigation.state.params.currentList.id].backgroundColor : this.props.lists[this.props.navigation.state.params.currentList.id - 2].backgroundColor
    }
  }

  componentWillUnmount(){
    this._panel.hide()
  }

  handleCancel = () => {
    this.setState({ todoDialogVisible: false });
  };
  handleAddTask = () => {
    let listId = this.state.listId
    console.log(this.state.listId)
    if (this.state.todoTitle) {
      let id = this.props.lists[this.state.listId - 1].todos.length + 1
      let todo = {
        id: id,
        title: this.state.todoTitle,
        description: this.state.todoDescription,
        done: false
      }
      this.props.dispatchAddTodo(this.state.listId, todo)
      this.setState({ todoDialogVisible: false, listId: '', todoTitle: '', todoDescription: '' });
    } else {
      this.setState({ todoDialogVisible: false })
    }

  };


  showDialog = (listId) => {
    console.log(listId)
    this.setState({ todoDialogVisible: true, listId: listId });
  };

  render() {
    return (
      <View style={[styles.container]}>
        <View style={{ flexDirection: 'row' }}>
        </View>
        <Dialog.Container visible={this.state.todoDialogVisible}>
          <Dialog.Title>Add Todo</Dialog.Title>
          <Dialog.Input placeholder="Todo Title" value={this.state.todoTitle} onChangeText={(todoTitle) => this.setState({ todoTitle })} />
          <Dialog.Input numberOfLines={5} placeholder="Todo Description" style={{ height: hp('20%') }} multiline value={this.state.todoDescription} onChangeText={(todoDescription) => this.setState({ todoDescription })} />

          <Dialog.Button label="Cancel" onPress={this.handleCancel} />
          <Dialog.Button label="Add" onPress={this.handleAddTask} />
        </Dialog.Container>
        <View style={[styles.bodyViewStyle, { backgroundColor: this.state.currentListColor, padding: hp(5) }]}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity ><Text style={{ color: this.state.activeList.textColor, alignSelf: 'flex-start', fontWeight: 'bold', fontSize: hp('3'), paddingBottom: '5%', width: wp('70%') }} numberOfLines={1} onPress={() => this._panel.hide()}>{this.state.activeList.title}</Text></TouchableOpacity>
            <TouchableOpacity onPress={() => this.showDialog(this.state.activeList.id)}><Icon name="plus" style={{ color: this.state.activeList.iconColor, alignSelf: 'flex-end', right: 0, fontSize: hp('4'), paddingBottom: '6%' }} /></TouchableOpacity>
          </View>
          <ScrollView style={{ paddingLeft: hp(2) }} showsVerticalScrollIndicator={false}>
            {this.props.lists[this.props.navigation.state.params.currentList.id - 1].todos.map((todo) => {
              return <TodoItemWithDescription data={todo} textColor={this.props.navigation.state.params.currentList.textColor} key={todo.id} listId={this.props.navigation.state.params.currentList.id} />
            })}
          </ScrollView>

        </View>
        <SlidingUpPanel ref={c => (this._panel = c)} showBackdrop={false} draggableRange={{ top: hp(75), bottom: hp(20) }}>
          {dragHandler => (
            <View style={[style.container, { backgroundColor: this.state.nextListColor, borderTopRightRadius: 40 }]}>

              <View style={style.dragHandler} {...dragHandler}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <TouchableOpacity ><Text style={{ color: this.state.nextList.textColor, alignSelf: 'flex-start', fontWeight: 'bold', fontSize: hp('3'), paddingBottom: '5%', width: wp('70%') }} numberOfLines={1} onPress={() => this._panel.show(hp('75%'))}>{this.state.nextList.title}</Text></TouchableOpacity>
                  <TouchableOpacity onPress={() => this.showDialog(this.state.nextList.id)}><Icon name="plus" style={{ color: this.state.nextList.iconColor, alignSelf: 'flex-end', right: 0, fontSize: hp('4'), paddingBottom: '6%' }} /></TouchableOpacity>
                </View>
              </View>
              <ScrollView style={{ paddingLeft: wp(15), marginTop: hp(-2.2)}} showsVerticalScrollIndicator={false}>
                {this.props.lists[this.state.nextList.id - 1].todos.map((todo) => {
                  return <TodoItemWithDescription data={todo} textColor={this.state.nextList.textColor} key={todo.id} listId={this.state.nextList.id} />
                })}
              </ScrollView>
            </View>
          )}
        </SlidingUpPanel>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    lists: state.todos.lists
  }
};
function mapDispatchToProps(dispatch) {
  return {
    dispatchAddTodo: (listId, todo) => dispatch(addTodo(listId, todo)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);

const style = {
  container: {
    flex: 1,
    zIndex: 1,
    backgroundColor: 'white',

  },
  dragHandler: {
    height: hp(14),
    padding: hp(5),
    alignSelf: 'stretch',
    borderTopRightRadius: 40,
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F1F5F6'
  },
  bodyViewStyle: {
    flex: 1,
    marginTop: hp(1),

    borderTopRightRadius: 40,
    backgroundColor: 'white',
    marginBottom: hp(15)
  },
});
