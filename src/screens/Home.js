import React, { Component } from 'react';
import { Dimensions, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import Dialog from "react-native-dialog";
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MCIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { addList, addTodo } from '../actions/index';
import Clock from '../components/Clock';
import GreetingText from '../components/GreetingText/GreetingText';
import ListContainer from '../components/ListContainer/ListContainer';
import { ScrollView } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('window');


const colors = ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#f1c40f', '#e67e22', '#e74c3c']

const styles = {
    container: {
        flex: 1,
        zIndex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    dragHandler: {
        alignSelf: 'stretch',
        height: 64,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ccc'
    }
}
class Home extends Component {
    static navigationOptions = {
        headerStyle: {
            backgroundColor: '#F1F5F6',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
                height: 0,
            },
            elevation: 0,
            borderBottomWidth: 0,
        },
        headerLeft: <MCIcons name='menu' style={{ color: '#93a7b8', fontSize: 25, paddingLeft: 20 }} />,
        headerRight: <View style={{ paddingRight: 20 }}>
            <TouchableOpacity style={{ height: 30, width: 30, padding: 3, borderRadius: 15, alignContent: 'center' }} >
                <Image source={require('../../assets/user.png')} style={{ width: 24, height: 24, alignSelf: 'center' }} />
            </TouchableOpacity>
        </View>
    };
    constructor(props) {
        super(props)

        this.state = {
            listId: '',
            todoTitle: '',
            listTitle: '',
            todoDescription: '',
            todoDialogVisible: false,
            addListDialogVisible: false,
        }
    }


    handleCancel = () => {
        this.setState({ todoDialogVisible: false });
    };

    handleListCancel = () => {
        this.setState({ addListDialogVisible: false });
    }

    handleAddTask = () => {
        let listId = this.state.listId
        console.log(this.state.listId)
        if (this.state.todoTitle) {
            let id = this.props.lists[this.state.listId - 1].todos.length + 1
            let todo = {
                id: id,
                title: this.state.todoTitle,
                description: this.state.todoDescription,
                done: false
            }
            this.props.dispatchAddTodo(this.state.listId, todo)
            this.setState({ todoDialogVisible: false, listId: '', todoTitle: '', todoDescription: '' });
        } else {
            this.setState({ todoDialogVisible: false })
        }

    };


    showDialog = (listId) => {
        console.log(listId)
        this.setState({ todoDialogVisible: true, listId: listId });
    };
    showListDialog = () => {
        this.setState({ addListDialogVisible: true });
    };

    addList = () => {
        if (this.state.listTitle) {
            var randomColor = Math.floor(Math.random() * colors.length);
            let list = {
                id: this.props.lists.length + 1,
                title: this.state.listTitle,
                backgroundColor: colors[randomColor],
                iconColor: 'white',
                textColor: 'white',
                todos: []
            }
            this.props.dispatchAddList(list)
            this.setState({ listTitle: '', addListDialogVisible: false })
        }
    }

    toggleTodo = (listId, todoId, status) => {
        this.props.dispatchToggleTodo(listId, todoId, status)
    }
    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#F1F5F6', paddingTop: '3%' }}>
                <Dialog.Container visible={this.state.todoDialogVisible}>
                    <Dialog.Title>Add Todo</Dialog.Title>
                    <Dialog.Input placeholder="Todo Title" value={this.state.todoTitle} onChangeText={(todoTitle) => this.setState({ todoTitle })} />
                    <Dialog.Input numberOfLines={5} placeholder="Todo Description" style={{ height: hp('20%') }} multiline value={this.state.todoDescription} onChangeText={(todoDescription) => this.setState({ todoDescription })} />

                    <Dialog.Button label="Cancel" onPress={this.handleCancel} />
                    <Dialog.Button label="Add" onPress={this.handleAddTask} />
                </Dialog.Container>
                <Dialog.Container visible={this.state.addListDialogVisible}>
                    <Dialog.Title>Add Task List</Dialog.Title>
                    <Dialog.Input placeholder="List Title" value={this.state.listTitle} onChangeText={(listTitle) => this.setState({ listTitle })} />

                    <Dialog.Button label="Cancel" onPress={this.handleListCancel} />
                    <Dialog.Button label="Add" onPress={this.addList} />
                </Dialog.Container>
                <GreetingText />
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingBottom: hp(3) }}>
                    <Clock />
                </View>

                <View style={{ paddingRight: hp(2), paddingBottom: hp(2), paddingLeft: hp(2), justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Text style={{ fontSize: hp(3), alignSelf: 'flex-start', fontWeight: 'bold', color: '#6A859D' }}>Tasks <Text style={{ color: 'List', fontWeight: '400' }}>List</Text></Text>
                    <TouchableOpacity style={{ backgroundColor: '#FF465B', borderRadius: 5, height: 25, width: 25, justifyContent: 'center' }} onPress={() => this.showListDialog()}><Text style={{ textAlign: 'center', color: 'white' }}>+</Text></TouchableOpacity>
                </View>
                <FlatList
                    horizontal
                    extraData={this.props}
                    data={this.props.lists}
                    renderItem={({ item: rowData }) => {
                        return (
                            <ListContainer data={rowData} showDialog={this.showDialog} navigation={this.props.navigation}  lists={this.props.lists} />
                        );
                    }}
                    style={{height: hp(47)}}
                    keyExtractor={(item, index) => index}
                />

            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        lists: state.todos.lists
    }
};
function mapDispatchToProps(dispatch) {
    return {
        dispatchAddList: (list) => dispatch(addList(list)),
        dispatchAddTodo: (listId, todo) => dispatch(addTodo(listId, todo)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
