import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import todoReducer from './reducers/index'

import Home from './screens/Home'
import DetailsScreen from './screens/DetailsScreen'

const store = createStore(todoReducer)
const AppNavigator = createStackNavigator({
    Home: Home,
    DetailsScreen: DetailsScreen
  },{
    initialRouteName: "Home",
  })

  const AppContainer = createAppContainer(AppNavigator);


  export default () => 
    <Provider store={ store }>
        <AppContainer />
      </Provider>