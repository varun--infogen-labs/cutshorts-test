import { ADD_LIST, ADD_TODO, TOGGLE_TODO } from './actionTypes'

export function addList(list) {
    return {
        type: 'ADD_LIST',
        list
    }
}

export function addTodo(listId, todo) {
    console.log(listId)
    return {
        type: 'ADD_TODO',
        listId,
        todo
    }
}

export function toggleTodo(listId, todoId, status) {
    console.log(listId)
    return {
        type: 'TOGGLE_TODO',
        listId,
        todoId,
        status
    }
}