import update from 'immutability-helper';
import * as types from '../actions/actionTypes';

const initialState = {
  lists:
    [
      {
        id: 1,
        title: 'Daily Task',
        backgroundColor: 'white',
        iconColor: '#1C9AFF',
        textColor: '#8BA1B3',
        todos: [
          {
            id: 1,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: true
          },
          {
            id: 2,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: false
          }, {
            id: 3,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: false
          }, {
            id: 4,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: false
          }
        ]
      }, {
        id: 2,
        title: 'Tense Tuesday',
        backgroundColor: '#1C9AFF',
        iconColor: 'white',
        textColor: 'white',
        todos: [
          {
            id: 1,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: false
          },
          {
            id: 2,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: false
          }, {
            id: 3,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: false
          }, {
            id: 4,
            title: 'Pick up laundry',
            description: "Pick up my laundry from Coin's",
            done: false
          }
        ]
      }
    ]
}


const todos = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_LIST:
      return {
        lists: [...state.lists, action.list]
      }

    case types.ADD_TODO:
      return update(state, {
        "lists": {
          [action.listId - 1]: {
            "todos": {
              $push: [action.todo]
            }
          }
        }
      })

    case types.TOGGLE_TODO:
    console.log(action)
      return update(state, {
        "lists": {
          [action.listId - 1]: {
            "todos": {
              [action.todoId -1 ]: {
                "done": {
                  $set: action.status
                }
              }
            }
          }
        }
      })
      
    default:
      return state;
  }

}

export default todos;