import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import MCIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { toggleTodo } from '../actions/index';

class TodoItemWithDescription extends Component {
    componentDidMount() {
        console.log(this.props)
    }

    toggleTodo = (status) => {
        toggleTodo(this.props.listId, this.props.data.id, status)
    }

    render() {
        return (
            <View key={this.props.data.id}>
                {!this.props.data.done ? <View>
                    <TouchableOpacity onPress={() => this.props.dispatchToggleTodo(this.props.listId, this.props.data.id, !this.props.data.done)} style={{ flexDirection: 'row', paddingTop: '4%' }}>
                        <MCIcons name='checkbox-blank-outline' style={{ color: this.props.textColor, paddingRight: '4%', fontSize: hp(3.2) }} />
                        <Text style={{ color: this.props.textColor, fontSize: hp(2.5) }}>{this.props.data.title}</Text>
                    </TouchableOpacity>
                    <Text style={{ color: this.props.textColor, fontSize: hp(1.2), paddingLeft: wp(10), paddingBottom: hp(1), width: wp(45) }}>{this.props.data.description}</Text>

                </View> : <TouchableOpacity onPress={() => this.props.dispatchToggleTodo(this.props.listId, this.props.data.id, !this.props.data.done)} style={{ flexDirection: 'row', paddingTop: '4%' }}>
                        <MCIcons name='checkbox-marked' style={{ color: this.props.textColor, paddingRight: '4%', fontSize: hp(3.2) }} />
                        <Text style={{ color: this.props.textColor, fontSize: hp(2.5), textDecorationLine: 'line-through' }}>{this.props.data.title}</Text>
                    </TouchableOpacity>}
            </View>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchToggleTodo: (listId, todoId, status) => dispatch(toggleTodo(listId, todoId, status))
    }
}

export default connect(null, mapDispatchToProps)(TodoItemWithDescription)