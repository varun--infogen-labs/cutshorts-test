import React, { Component } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import TodoItem from '../TodoItem/TodoItem';
export default class ListContainer extends Component {
    constructor(props){
        super(props)

        this.state ={
            list: this.props.data
        }
    }

    componentWillReceiveProps(NextProps){
        console.log(NextProps)
    }

    
    render(){
        return (
            <View style={{ marginRight: wp('4%'), marginLeft: wp('4%') }} key={this.props.data.id} key={this.props.data.id}>
                <View style={{ top: wp('3.5%'), left: wp('3.5%'), borderWidth: 1, borderRadius: 15, width: wp('60%'), padding: '5%', backgroundColor: '#E0E7EF', borderColor: '#E0E7EF', height: hp('40%') }}>

                </View>
                <View style={{ top: hp('-40%'), borderWidth: 1, borderRadius: 15, width: wp('60%'), padding: '10%', backgroundColor: this.props.data.backgroundColor, borderColor: '#E0E7EF', height: hp('40%') }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailsScreen', {currentList: this.props.data})}><Text style={{ color: this.props.data.textColor, alignSelf: 'flex-start', fontWeight: 'bold', fontSize: hp('2'), paddingBottom: '5%', width: wp('40%') }} numberOfLines={1}>{this.props.data.title}</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.showDialog(this.props.data.id)}><Icon name="plus" style={{ color: this.props.data.iconColor, alignSelf: 'flex-end', right: 0, fontSize: hp('2'), paddingBottom: '6%' }} /></TouchableOpacity>
                    </View>
                    <ScrollView>
                    {this.props.data.todos.map((todo) => {
                        return <TodoItem data={todo} textColor={this.props.data.textColor} key={todo.id} listId={this.props.data.id}/>
                    })}
                    </ScrollView>
                </View>

            </View>
        )
    }
}
