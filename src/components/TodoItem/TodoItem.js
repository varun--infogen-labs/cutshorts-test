import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/AntDesign'
import MCIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {toggleTodo} from '../../actions/index'
import { connect } from 'react-redux';

class TodoItem extends Component {
    componentDidMount(){
        console.log(this.props)
    }

    toggleTodo =(status) =>{
        toggleTodo(this.props.listId, this.props.data.id, status)
    }

    render() {
        return (
            <View key={this.props.data.id}>
                {!this.props.data.done ? <TouchableOpacity onPress={() => this.props.dispatchToggleTodo(this.props.listId, this.props.data.id, !this.props.data.done)} style={{ flexDirection: 'row', paddingTop: '4%' }}>
                    <MCIcons name='checkbox-blank-outline' style={{ color: this.props.textColor, paddingRight: '4%', fontSize: 18 }} />
                    <Text style={{ color: this.props.textColor, fontSize: 15 }}>{this.props.data.title}</Text>
                </TouchableOpacity> : <TouchableOpacity onPress={() => this.props.dispatchToggleTodo(this.props.listId, this.props.data.id, !this.props.data.done)} style={{ flexDirection: 'row', paddingTop: '4%' }}>
                        <MCIcons name='checkbox-marked' style={{ color: this.props.textColor, paddingRight: '4%', fontSize: 18 }} />
                        <Text style={{ color: this.props.textColor, fontSize: 15, textDecorationLine: 'line-through' }}>{this.props.data.title}</Text>
                    </TouchableOpacity>}
            </View>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchToggleTodo: (listId, todoId, status) => dispatch(toggleTodo(listId, todoId, status))
    }
}

export default connect(null, mapDispatchToProps)(TodoItem)