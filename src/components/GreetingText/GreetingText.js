import moment from "moment";
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

export default class GreetingText extends Component {
    constructor(props) {
        super(props)

        this.state = {
            greetingTest: ''
        }
    }

    componentDidMount() {
        let now = moment()
        const currentHour = now.hour()
        console.log(currentHour)
        const splitAfternoon = 12; // 24hr time to split the afternoon
        const splitEvening = 17; // 24hr time to split the evening

        if (currentHour >= splitAfternoon && currentHour <= splitEvening) {
            // Between 12 PM and 5PM
            this.setState({ greetingText: 'Good Afternoon,' })
        } else if (currentHour >= splitEvening) {
            // Between 5PM and Midnight
            this.setState({ greetingText: 'Good Evening,' })
        } else {
            // Between dawn and noon
            this.setState({ greetingText: 'Good Morning,' })
        }
    }

    render() {
        return (
            <View style={{ paddingLeft: wp(5), paddingBottom: hp(2) }}>
                <Text style={{ fontSize: wp(7), color: '#8BA0B2' }}>{this.state.greetingText}</Text>
                <Text style={{ marginTop: wp(0.5), color: '#46AAFD', fontSize: wp(5) }}>Varun Makhija</Text>
            </View>
        )
    }
}