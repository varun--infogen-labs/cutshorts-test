import { applyMiddleware, createStore } from 'redux';
import {autoRehydrate} from 'redux-persist'

import rootReducer from '../reducers';
import { logger } from 'redux-logger';

export default function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    autoRehydrate(),
    applyMiddleware(logger)
  );
}